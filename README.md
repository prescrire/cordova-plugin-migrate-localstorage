# Migrate LocalStorage

This plugin migrates localStorage and IndexedDB when migrating from `cordova-ios` <-= `5.x.x` to `cordova-ios` >= `6.x.x` which are using `WKWebView`.

Based on https://github.com/mushishi78/cordova-plugin-migrate-localstorage.

## Configuration

The plugin uses standard configuration of `cordova-ios` for `WKURLSchemeHandler`. The preferences define where to migrate. If `scheme` and `host` are not defined, `file` scheme will be used (a default scheme for `cordova-ios` <-= `5.x.x`)

```
<preference name="host" value="YOUR-HOST-TO-MIGRATE-TO" />
<preference name="scheme" value="YOUR-SCHEME-TO-MIGRATE-TO"/>
```

You can also define the source for migration. By default, `file` scheme will be used (a default scheme for `cordova-ios` <-= `5.x.x`)

```
<preference name="migrate-host" value="YOUR-HOST-TO-MIGRATE-FROM" />
<preference name="migrate-scheme" value="YOUR-SCHEME-TO-MIGRATE-FROM"/>
```

## How to use

```sh
cordova plugin add git clone https://bitbucket.org/prescrire/cordova-plugin-migrate-localstorage.git
```
