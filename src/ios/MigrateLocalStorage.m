/** Code Adapted from https://github.com/mushishi78/cordova-plugin-migrate-localstorage */

#import "MigrateLocalStorage.h"

#define TAG @"\nMigrateLS"

@implementation MigrateLocalStorage

void move(NSFileManager *fileManager, NSString *src, NSString *dest)
{
    if (![fileManager fileExistsAtPath:src]) {
        NSLog(@"%@ source file does not exist", TAG);
        return;
    }

    if ([fileManager fileExistsAtPath:dest]) {
        NSLog(@"%@ target file exists", TAG);
        return;
    }

    if (![fileManager createDirectoryAtPath:[dest stringByDeletingLastPathComponent] withIntermediateDirectories:YES attributes:nil error:nil]) {
        NSLog(@"%@ error creating target file", TAG);
        return;
    }

    NSError *err;
    BOOL success = [fileManager moveItemAtPath:src toPath:dest error:&err];
    if (!success) {
        NSLog(@"%@ error moving path: %@ ", TAG, err.localizedDescription);
    }
}

void migrateLocalStorage(NSFileManager *fileManager, NSString *localStorageFolder, NSString *sourceDomain, NSString *targetDomain)
{
    NSString *source = [NSString stringWithFormat:@"%@/%@.localstorage", localStorageFolder, sourceDomain];
    NSString *target = [NSString stringWithFormat:@"%@/%@.localstorage", localStorageFolder, targetDomain];
    if ([fileManager fileExistsAtPath:target]) {
        NSLog(@"%@ localStorage already migrated to %@", TAG, targetDomain);
        return;
    }
    if (![fileManager fileExistsAtPath:source]) {
        NSLog(@"%@ localStorage already migrated from %@", TAG, sourceDomain);
        return;
    }
    NSLog(@"%@ migrating localStorage", TAG);
    NSLog(@"%@ source %@", TAG, source);
    NSLog(@"%@ target %@", TAG, target);
    move(fileManager, source, target);
    move(fileManager, [source stringByAppendingString: @"-shm"], [target stringByAppendingString: @"-shm"]);
    move(fileManager, [source stringByAppendingString: @"-wal"], [target stringByAppendingString: @"-wal"]);
    NSLog(@"%@ localStorage migration succeeded!", TAG);
}

void migrateIndexedDB(NSFileManager* fileManager, NSString* indexedDBFolder, NSString *sourceDomain, NSString *targetDomain)
{
    NSString *source = [NSString stringWithFormat:@"%@/v1/%@", indexedDBFolder, sourceDomain];
    NSString *target = [NSString stringWithFormat:@"%@/v1/%@", indexedDBFolder, targetDomain];
    if ([fileManager fileExistsAtPath:target]) {
        NSLog(@"%@ IndexedDB already migrated to %@", TAG, targetDomain);
        return;
    }
    if (![fileManager fileExistsAtPath:source]) {
        NSLog(@"%@ IndexedDB already migrated from %@", TAG, sourceDomain);
        return;
    }
    NSLog(@"%@ migrating indexedDB", TAG);
    NSLog(@"%@ original %@", TAG, source);
    NSLog(@"%@ target %@", TAG, target);
    move(fileManager, source, target);
    NSLog(@"%@ IndexedDB migration succeeded!", TAG);
}

NSString *getDomainFromSettings(NSDictionary *settings, NSString *schemeKey, NSString *hostnameKey)
{
   return [NSString stringWithFormat:@"%@_%@_0",
           [settings cordovaSettingForKey:schemeKey] ?: @"file",
           [settings cordovaSettingForKey:hostnameKey] ?: @""];
}

- (void)pluginInitialize
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *appLibraryFolder = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *bundleIdentifier = nil;
#if TARGET_IPHONE_SIMULATOR
    bundleIdentifier = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"];
#endif
    NSString *webKitFolder = bundleIdentifier ? [@"WebKit" stringByAppendingPathComponent: bundleIdentifier] : @"WebKit";
    NSString *websiteDataFolder = [NSString stringWithFormat:@"%@/%@/WebsiteData", appLibraryFolder, webKitFolder];
    NSString *localStorageFolder = [websiteDataFolder stringByAppendingPathComponent:@"LocalStorage"];
    NSString *indexedDBFolder = [websiteDataFolder stringByAppendingPathComponent:@"IndexedDB"];
    NSDictionary* settings = self.commandDelegate.settings;

    NSString *sourceDomain = getDomainFromSettings(settings, @"migrate-scheme", @"migrate-hostname");
    NSString *targetDomain = getDomainFromSettings(settings, @"scheme", @"hostname");

    migrateLocalStorage(fileManager, localStorageFolder, sourceDomain, targetDomain);
    migrateIndexedDB(fileManager, indexedDBFolder, sourceDomain, targetDomain);
}

@end
